import yfinance as yf
import pandas as pd
import numpy as np
import datetime


def init():
    try:
        start = datetime.datetime.now() - datetime.timedelta(days=10)
        end = datetime.datetime.now()
        file_path = "sma.csv"
        all_scrips = pd.read_csv(file_path)
        all_scrips = all_scrips.reset_index()
        all_results = pd.DataFrame()
        for index, row in all_scrips.iterrows():
            try:
                scrip_name = row['scrip_name']
                stock_data = yf.download(scrip_name, start=start, end=end)
                cmp = stock_data["Close"].tail(1).values
                updated_cmp = cmp[0]
                row['qty'] = 10
                row['invested_amount'] = row['cmp'] * row['qty']
                row['updated_cmp'] = updated_cmp
                difference_in_cmp = row['updated_cmp'] - row['cmp']
                row['percentage_change'] = (difference_in_cmp/row['cmp'])*100
                row['p_l'] = difference_in_cmp
                all_results = all_results.append(
                    row, ignore_index=True)
                print(row)
                print(all_results)
            except:
                print("Error in performing analysis for scrip:", scrip_name)
        all_results.to_csv("Portfolio Updated-{}.csv".format(end), index=False)
    except Exception as e:
        print("Error in performing individual analysis", e)
        err_message = "no_data"
        return err_message


init()
