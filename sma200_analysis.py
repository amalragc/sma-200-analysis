import yfinance as yf
import pandas as pd
import numpy as np
import datetime


def buy_call_generator(ticker):
    # Get the data for the last year
    start = datetime.datetime.now() - datetime.timedelta(days=365)
    end = datetime.datetime.now()
    stock_data = yf.download(ticker, start=start, end=end)
    print("stock data:", stock_data)
    # Calculate the SMA 200
    stock_data['SMA_200'] = stock_data['Close'].rolling(window=200).mean()
    cmp = stock_data["Close"].tail(1).values
    cmp = cmp[0]
    print("cmp:", cmp)
    print(stock_data)
    last_day = stock_data['SMA_200'].tail(2).values
    last_day_sma = last_day[0]
    print("cmp:", cmp)
    print("sma 200:", last_day_sma)
    sma_plus_10_perc = last_day_sma + (0.1*last_day_sma)
    print("sma + 10 %:", sma_plus_10_perc)
    if(cmp <= sma_plus_10_perc and cmp > last_day_sma):
        print("BUY")
        signal = "BUY"
    elif(cmp < last_day_sma):
        print("Below SMA")
        signal = "Below SMA"
    else:
        print("No Action")
        signal = "Over Value"
    analysis_results = {
        "signal": signal,
        "sma_200": last_day_sma,
        "sma_plus_10_perc": sma_plus_10_perc,
        "cmp": cmp
    }

    return (analysis_results)


def init():
    try:
        file_path = "nifty_50.csv"
        all_scrips = pd.read_csv(file_path)
        all_scrips = all_scrips.reset_index()
        all_results = pd.DataFrame()
        for index, row in all_scrips.iterrows():
            try:
                scrip_name = row['yfinance_id']
                company_name = row['company_name']
                if ('sector_name' in all_scrips.columns):
                    sector_name = row['sector_name']
                else:
                    sector_name = "NaN"
                res = buy_call_generator(scrip_name)
                if(res == "no_data"):
                    print("No data for this scrip:", scrip_name)
                    continue
                single_res = {
                    'scrip_name': scrip_name,
                    'company_name': company_name,
                    'sector_name': sector_name,
                    'signal': res['signal'],
                    'cmp': res['cmp'],
                    'sma_200': res['sma_200'],
                    'sma_plus_10_perc': res['sma_plus_10_perc']
                }
                single_res_df = pd.DataFrame(single_res, index=[0])
                all_results = pd.concat([all_results, single_res_df], ignore_index=True)
                print(all_results)
                now = datetime.datetime.now()
                print("Current time:", now.time())
            except Exception as e:
                print("Error in performing analysis for scrip:", scrip_name,e)
        all_results.to_csv("SMA 200 analysis.csv", index=False)
    except:
        print("Error in performing individual analysis")
        err_message = "no_data"
        return err_message


init()
