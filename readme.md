# SMA 200 on Nifty50 Components

- Python script to run sma-200 analysis on nifty50 components.<br>
- It will give buy calls when a scrip starts to climb above sma200 levels. <br>
- To add more scrips, edit the file nifty_50.csv. <br>
- scrip id's can be obatained from [Yahoo finance](https://finance.yahoo.com/quote/WIPRO.NS) website <br>
